// #####################################################################################
// ## Programa: Jogo de Adivinhação - Client                                          ##
// ## Autor: Erick de Almeida Calegaro                                                ##
// ## Versao: 1.00                                                                    ##
// ## Concluída em: 24/03/2021                                                        ##
// ## Descricao:                                                                      ##
// ##      Este é um jogo de adivinhação simples onde um server gera um número        ##
// ##      aleatório e o usuário no client tenta adivinhá-lo com base em feedbacks    ##
// #####################################################################################

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include "header.h"
   
int main(int argc, char const *argv[])
{
    int rc = RC_OKAY;

    system("clear");

    if (argc > 3)
        rc = RC_MANY_ARGS;
    else if (argc < 3)
        rc = RC_FEW_ARGS;

    if (rc == RC_OKAY)
        rc = doProcessSocketClient(argv[1], argv[2]);
    
    return debugErrorsClient(rc);
}

int doProcessSocketClient(char const *ip, char const *sPort)
{

    struct  sockaddr_in serv_addr;
    char    buffer   [1024] = {0x00};
    char    response [1024] = {0x00};
    char    randNum     [2] = {0x00};
    int     sock            = 0;
    int     valread         = 0;
    int     attempts        = 0;
    int     iPort           = atoi(sPort);

    //O range de portas TCP é entre 0 e 65536, valores fora do range estao incorretos
    if (iPort < 0)
        return RC_LOW_PORT_VALUE;
    else if (iPort > 65536)
        return RC_HIGH_PORT_VALUE;

    //Criar Socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        return RC_SOCKET_CREATE;
   
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(iPort);
       
    //Converter IPv4 e IPv6 Asc2Bin
    if(inet_pton(AF_INET, ip, &serv_addr.sin_addr) <= 0) 
        return RC_INET_ADDRESS;

    printf("Conectando-se a %s na porta TCP %d\n", ip, iPort);
   
    //Conectar ao servidor de destino
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        return RC_SOCKET_CONNECT;

    printf("Conexão estabelecida!\n");

    //Receber numero aleatorio gerado pelo servidor
    valread = read(sock, randNum, 2);

    //Executar a Logica do jogo
    attempts = doProcessGame(randNum);

    //Enviar a mensagem informando o servidor do resultado
    sprintf(response, "O usuário adivinhou o número aleatório %s usando %d tentativas\n", randNum, attempts);
    send(sock, response, strlen(response), 0);

    return RC_OKAY;
}

int doProcessGame(char * sRandNum)
{
    BOOL isCorrect = FALSE;
    int  iRandNum  = atoi(sRandNum);
    int  attempts  = 0;
    int  inputNum  = 0;

    do
    {
        printf("Digite um numero entre 0 e 10:\n>"); 
        scanf("%d", &inputNum);
        
        if (inputNum < iRandNum){
            printf("%d é muito baixo\n", inputNum);
        }
        else if (inputNum > iRandNum){
            printf("%d é muito alto\n", inputNum);
        }
        else{
            printf("Essa é a resposta correta!\n");
            isCorrect = TRUE;
        }

        attempts++;

    }while(!isCorrect); 

    return attempts;
}

int debugErrorsClient(int rc)
{
    switch(rc)
    {
        case RC_MANY_ARGS       : printf("Foram passados muitos argumentos na chamada\nUtilize \"./client <ip> <porta>\".\n");  break;
        case RC_FEW_ARGS        : printf("Foram passados poucos argumentos na chamada\nUtilize \"./client <ip> <porta>\".\n");  break;
        case RC_LOW_PORT_VALUE  : printf("Valor da porta está muito baixo, deve ser maior ou igual a 0.\n");                    break;
        case RC_HIGH_PORT_VALUE : printf("Valor da porta está muito alto, deve ser menor ou igual a 65536.\n");                 break;
        case RC_SOCKET_CREATE   : printf("Falha ao criar socket!\n");                                                           break;
        case RC_SOCKET_CONNECT  : printf("Falha de Conexão!\n");                                                                break;
        case RC_INET_ADDRESS    : printf("Endereço IP invalido ou não suportado\n");                                            break;
    }

    return rc;
}

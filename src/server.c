// #####################################################################################
// ## Programa: Jogo de Adivinhação - Server                                          ##
// ## Autor: Erick de Almeida Calegaro                                                ##
// ## Versao: 1.00                                                                    ##
// ## Concluída em: 24/03/2021                                                        ##
// ## Descricao:                                                                      ##
// ##      Este é um jogo de adivinhação simples onde um server gera um número        ##
// ##      aleatório e o usuário no client tenta adivinhá-lo com base em feedbacks    ##
// #####################################################################################

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <time.h>
#include "header.h"

int main(int argc, char const *argv[])
{
    int rc = RC_OKAY;

    system("clear");

    if (argc > 2)
        rc = RC_MANY_ARGS;
    else if (argc < 2)
        rc = RC_FEW_ARGS;

    if (rc == RC_OKAY)
        rc = doProcessSocketServer(argv[1]);
    
    return debugErrorsServer(rc);
}

int doProcessSocketServer(char const *sPort)
{
    struct sockaddr_in address;
    char   buffer   [1024] = {0x00};
    char   randNum  [2]    = {0x00};
    int    opt             = 1;
    int    iPort           = atoi(sPort);
    int    addrlen         = sizeof(address);
    int    server_fd, new_socket, valread;

    //O range de portas TCP é entre 0 e 65536, valores fora do range estao incorretos
    if (iPort < 0)
        return RC_LOW_PORT_VALUE;
    else if (iPort > 65536)
        return RC_HIGH_PORT_VALUE;
  
    printf("Servidor iniciado na porta TCP %d\n", iPort);

    //Criar socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
        return RC_SOCKET_CREATE;
       
    //Parametrizacao de comportamento
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
        return RC_SOCKET_OPTIONS ;

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(iPort);
       
    //Atachar o endereço
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
        return RC_SOCKET_ATACH;
    
    printf("Aguardando uma conexão...\n");

    //Escutando...
    if (listen(server_fd, 3) < 0)
        return RC_SOCKET_LISTEN;

    //Aceita nova conexão de um cliente
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0)
        return RC_SOCKET_ACCEPT;

    printf("Conexão estabelecida!\n");

    //Criação de envio do numero aleatorio
    srand (time(NULL));
    sprintf(randNum, "%d", rand()%10);
    send(new_socket , randNum, strlen(randNum), 0);

    printf("Aguardando resposta do usuario...\n");

    //Lê dados enviados pelo client
    valread = read( new_socket , buffer, 1024);
    printf("%s\n",buffer );

    return RC_OKAY;
}

int debugErrorsServer(int rc)
{
    switch(rc)
    {
        case RC_MANY_ARGS       : printf("Foram passados muitos argumentos na chamada\nUtilize \"./server <porta>\".\n");   break;
        case RC_FEW_ARGS        : printf("Foram passados poucos argumentos na chamada\nUtilize \"./server <porta>\".\n");   break;
        case RC_LOW_PORT_VALUE  : printf("Valor da porta está muito baixo, deve ser maior ou igual a 0.\n");                break;
        case RC_HIGH_PORT_VALUE : printf("Valor da porta está muito alto, deve ser menor ou igual a 65536.\n");             break;
        case RC_SOCKET_CREATE   : printf("Falha ao criar socket!\n");                                                       break;
        case RC_SOCKET_OPTIONS  : printf("Falha ao definir opções no socket!\n");                                           break;
        case RC_SOCKET_ATACH    : printf("Falha ao atachar serviço na rede!\n");                                            break;
        case RC_SOCKET_LISTEN   : printf("Falha ao abrir escuta!\n");                                                       break;
        case RC_SOCKET_ACCEPT   : printf("Conexão recusada!\n");                                                            break;
    }

    return rc;
}